class RegistrationsController < Devise::RegistrationsController

  private

  # Devise parameter allowed for controller during signup
  def sign_up_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  # Devise parameter allowed for controller during update account
  def account_update_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :avatar, :website, :twitter)
  end

  # Allow Devise user to update the account without putting current password
  def update_resource(resource, params)
    resource.update_without_password(params)
  end

end