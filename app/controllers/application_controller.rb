class ApplicationController < ActionController::Base
  include Pundit

  protect_from_forgery with: :exception
  before_filter :set_search

  # rescue from pundit by going the function
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  # alert user for unauthorized usage
  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(request.referrer || root_path)
  end

  def set_search
    @q=Blog.search(params[:q])
  end
end
